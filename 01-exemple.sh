#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple de primer programa
# Normes:
# 	shebang (#!) indica qui ha d'interpretar el fitxer
# 	capçalera: descripcio, data, autor
# ----------------------------------------------------

echo "hola mon"
nom="pere pou prat"
edat=25
echo $nom $edat
echo -e "nom: $nom\n edat: $edat\n"
echo -e 'nom: $nom\n edat: $edat\n'
uname -a
uptime
echo $SHELL
echo $SHLVL
echo $((4*32))
echo $((edat * 2))

# read data1 data2
# echo -e "$data1 \n   $data2"	# LO QUE LE ESTAMOS DICIENDO ES QUE EL USUARIO HAGA UN INPUT
				# AUNQUE LOS SCRIPTS NO SON INTERACTIVOS

exit 0		# SI EJECUTAMOS EN LA TERMINAL ESTE PROGRAMA, SE NOS CERRARÍA LA PANTALLA		      # YA QUE LE ESTAMOS DICIENDO QUE SE NOS SALGA DE LA TERMINAL 
