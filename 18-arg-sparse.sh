#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Març 2023
# Data: 7-03-2023
# Curs:ASIX
# 17-arg_for.sh [ -a -b -c -d ] arg...
# -------------------------------------
            
opcions=""
arguments=""
fitxer=""
num=""
while [ "$1" ]
do
  case "$1" in
   "-b"|"-c"|"-e")
     opcions="$opcions $1";;
   "-a")
     opcions="$opcions $1"
     fitxer=$2
     shift;;
   "-d")
     opcions="$opcions $1"
     num=$2
     shift;; 
   *)
     arguments="$arguments $1";;	  
  esac	  
  shift  
done
echo "Opcions: $opcions"
echo "Arguments: $arguments"
echo "Fitxer: $fitxer"
echo "Num: $num"
exit 0
