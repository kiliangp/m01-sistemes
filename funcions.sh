#! /bin/bash
# Kilian Steven Guanoluisa Pionce

function showUid(){
    uid=$1
    linia=$(grep "^[^:]*:[^:]*:$uid:" /etc/passwd)
    login=$(echo $linia | cut -d: -f1)
    gid=$(echo $linia | cut -d: -f4)
    gecos=$(echo $linia | cut -d: -f5)
    home=$(echo $linia | cut -d: -f6)
    shell=$(echo $linia | cut -d: -f7)

    echo "login: $login"
    echo "uid: $uid"
    echo "gid: $gid"
    echo "gecos: $gecos"
    echo "home: $home"
    echo "shell: $shell"
}

function suma(){
    suma=$(($1+$2))
    echo $suma
  exit 0
}

function multiplica(){
    multi=$(($1*$2))
    echo $multi
    return 0
}

function showllistaUID(){
    for uid in $*
    do
      grep -q "^[^:]*:[^:]*:$uid:" /etc/passwd
      if [ $? -ne 0 ]; then
        echo "Error! UID $uid inexistent" >> /dev/stderr
      else
        showUid $uid
        echo "---------------"
      fi
    done
}

function informeShell(){
    llista_shells=$(cut -d: -f7 /etc/passwd | sort -u)
    for shell in $llista_shell
    do
      numlin=$(grep -c ":$shell$" /etc/passwd)
      if [ $numlin -ge 3 ]; then
        echo "Shell: $shell -------------"
        grep "$shell$" /etc/passwd | sed -r 's/^(.*)$/\t\1/'
      fi
    done
}

