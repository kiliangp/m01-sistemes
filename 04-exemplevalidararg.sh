#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple VALIDAR: tenemos que validar 1 nota.
#   $ prog edat
# ---------------------------------------
#
ERR_NARGS=1
ERR_NOTA=2
# 1) Si nums args no és correcte plegar
if [ $# -ne 1 ] 
then
  echo "Error: numero args incorrecte" # EN EL EXAMEN HAREMOS LAS FRASES MÁS DESARROLADAS
  echo "Usage: $0 nota "
  exit $ERR_NARGS	 		# SABEMOS QUE ES UN ERROR CUANDO ES != 0 			
fi

# 2) VALIDAR RANG NOTA 

if ! [ $1 -ge 0 -a $1 -le 10 ]
then
  echo "Error nota $1 no vàlida"
  echo "Nota pren valors de 0 a 10"
  echo "Usage: $0 nota"
  exit $ERR_NOTA
fi

# XIXA
nota=$1
if [ $nota -lt 5 ]
then 
  echo "nota $nota: Suspès"
else
  echo "nota $nota: Aprovat"
fi
exit 0
