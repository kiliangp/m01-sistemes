#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Març 2023
#
# Exemple FOR i IN: 
#   
# ---------------------------------------

# Estructura del for
    # for "variable" in _ _ _
    # do
        # accions
    # done

# 9 ) PASAMOS ARGUMENTOS (NO SABEMOS CUANTOS) Y LOS SUMA, NOS DICE EL TOTAL
if [ $# -eq 0 ]; then
  echo "ERROR: número d'arguments incorrecte"
  echo "USAGE: $0 num1 num2 num3..."
  exit 1
fi

suma=0

for arg in $*
do
  suma=$(($suma+$arg))
done 
echo $suma 
exit 0

# 8 ) LLISTAR TOTS ELS LOGINS NUMERATS
login=$(cut -d: -f1 /etc/passwd)            # OBTENEMOS LA LISTA DE LOS LOGIN
num=1

for usuaris in $login                       # 
do
  echo "$num: $usuaris"
  ((num++))                 # DE LAS DOS MANERAS ESTÁ BIEN 
done
exit 0


# 7 ) NUMERA LOS FICHEROS DEL DIRECTORIO ACTIVO 

num=1
llistat=$(ls)

for nom in $llistat
do
  echo "$num: $nom"
  ((num++))                # PODEMOS HACER ESTO YA QUE ANTES HEMOS DEFINIDO LA VARIABLE CON UN 1
done
exit 0


# 6 ) NUMERAR ELS ARGUMENTS

num=1

for arg in $*
do
  echo "$num: $arg"
  num=$((num+1))                # SUMAMOS UNO PARA QUE EMPIECE A ENUMERAR LOS ARGUMENTOS
done
exit 0 

    # EN EL EXAMEN NO APROBARÁN LOS ALUMNOS QUE NO SEPAN SI LAS FUNCIONES VAN DENTRO O NO DEL BUCLE
    # EN EL EXAMEN NO APROBARÁN LOS ALUMNOS QUE NO SEPAN SI DENTRO DE LOS "BRACERS" VA UN OPERADOR LÓGICO "-a" o "-o"




# 5 ) ITERAR I MOSTRAR LA LLISTA D'ARGUMENTS
for arg in "$*"             # NOS MOSTRARÁ TODOS LOS NOMBRES PERO EN LA MISMA LISTA
do
  echo "$arg"
done
exit 0 

    # EL $@ SE EXPANDIRÁ DE LAS DOS FORMAS, TANTO SI ESTÁ ENCAPSULADO COMO SI NO


# 4) ITERAR Y MOSTRAR LA LLISTA D'ARGUMENTS
    # LAS VARIABLES TIENEN QUE TENER NOMBRES QUE DEN INFORMACIÓN DEL PROGRAMA
for arg in $*               # NOS MOSTRARÁ UN ARGUMENTO POR LÍNEA
do
  echo "$arg"
done
exit 0 


# 3) ITERAR PEL VALOR D'UNA VARIABLE
llistat=$(ls)

for nom in $llistat
do
  echo "$nom"
done
exit 0


# 2) ITERAR PER UN CONJUNT D'ELEMENTS
for nom in "pere marta pau anna"
do
  echo "$nom"
done
exit 0
    # AQUÍ TE IMPRIMIRÁ UN ELEMENTO, QUE ESTE TIENE 4 ELEMENTOS EN SU INTERIOR
