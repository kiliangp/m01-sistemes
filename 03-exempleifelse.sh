#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple if: indicar si és major d'edat amb else 
#   $ prog edat
# ----------------------------------------
if [ $# -ne 1 ] 
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat" 	# LE ESTAMOS DICIENDO COMO TIENE QUE SER EL MENSAJE --> # EL #0 SIGNIFICA EL NOMBRE DEL PROGRAMA
  exit 1			# EN ESTE CASO EL OUTPUT SERÁ ESTE:
fi					# Error: numero args incorrecte
					# Usage: 03-exempleifelse.sh edat 

# 2) XIXA
edat=$1       # LE ESTAMOS DICIENDO QUE EDAT SEA EL PRIMER ARGUMENTO
if [ $edat -lt 18 ]	# EN EL INTERIOR DE LOS "BRACKETS" SIEMPRE TIENEN QUE TENER DOS ESPACIOS (1 DELANTE Y 1 DETRÁS)
then
  echo "edat $edat és menor d'edat"
else 			# NO HAY QUE PONER "CONDICIONES"
  echo "edat $edat és major d'edat" 
fi
exit 0
