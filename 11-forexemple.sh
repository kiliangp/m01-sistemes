#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Març 2023
#
# Exemple FOR i IN: 
#   
# ---------------------------------------

# Estructura del for
    # for "variable" in _ _ _
    # do
        # accions
    # done

for nom in "pere" "marta" "pau" "anna"
do
  echo "$nom"
done
exit 0
