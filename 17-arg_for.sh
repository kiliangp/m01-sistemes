#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Març 2023
# Data: 7-03-2023
# Curs:ASIX
# 17-arg_for.sh [ -a -b -c -d ] arg...
# -------------------------------------
            
ERR_ARGS=1 

if [ $# -eq 0 ]; 
then   
  echo "ERROR: numero args incorrecte"   
  echo "usage: $0 [-a -b -c -d] arg[...]"   
  exit $ERR_ARGS 
fi 

opcions="" 
arguments="" 

for arg in $* 
do   
  case $arg in   
  "-a"|"-b"|"-c"|"-d")       
      opcions="$opcions $arg";;   
  *)       
      arguments="$arguments $arg";;   
  esac 
done 
echo "Opcions: $opcions" 
echo "Arguments: $arguments" 
exit 0

