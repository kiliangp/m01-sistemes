# !/bin/bash
# Kilian Steven Guanoluisa Pionce
# Data: 7-03-2023
# Curs:ASIX
# Validar nota for

# --------------------------------------------------------------------------------------------------------------------------------------------------------------

ERR_NARGS=1
ERR_NUMS=2
OK=0

#Validar num args
if [ $# -lt 1 ]       # ERROR DESCUALIFICANTE 
then
  echo "Error: numero d'arguments invalid"
  echo "usage: prog.sh arg1"
  exit $ERR_NARGS
fi

llistanotes=$*
#Validar nota
for nota in $llistanotes    # HACEMOS UN BUCLE YA QUE NO SABEMOS EL NÚMERO DE ARGUMENTOS
#                           # POR LO QUE LO DECIDIRÁ EL USUARIO 
do
  if [ $nota -lt 0 -o $nota -gt 10 ]
  then
    echo "Error: La nota $nota no es valida" >> /dev/stderr           # ERROR RECUPERABLE  (NO DESCUALIFICANTE)
    echo "Usage: Posar un numero entre 1 y el 10" >> /dev/stderr      # SOLO PONDREMOS LA SALIDA DE ERROR EN LOS ERRORES RECUPERABLES 
    echo "Usage:  $ prog nota..." >> /dev/stderr                # LOS MENSAJES DE ERROR
  else
    if [ $nota -lt 5 ]
    then
      msg="$var: Suspes"
    elif [ $nota -lt 7 ]
    then
      msg="$var Suficient"
    elif [ $nota -le 9  ]
    then
      msg="$var Notable"
    else
      msg="$var Excelent"
    fi
    echo "La nota $nota és: $msg"
 fi
done
exit $OK

# A L'HORA DE EXECUTAR EL PROGRAMA, PODREM FER AIXÒ:
  # bash 12-fornotes.sh ... 2> error.log
    # TE APARECERÁN LAS NOTAS QUE ESTÁN BIEN
      # SI HACEMOS ESTA ORDEN
        # cat error.log
          # NOS APARECERÁ TODOS LAS NOTAS QUE HAN DADO ERROR
