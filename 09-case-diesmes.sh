#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple CASE: 
#   
# ---------------------------------------

# dl dt dc dj dv --> laborable
# ds dm  --> festiu
# altre
case $1 in
  "dl"|"dt"|"dc"|"dj"|"dv")
   # d[ltcjv]
   echo "$1 és un dia laborable";;
  "ds"|"dm")
   # d[sm]
    echo "1$ és festiu";;
  *)
    echo "això ($1) no és un dia";;
esac
exit 0
