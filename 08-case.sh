#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple CASE: 
#   
# ---------------------------------------


case $1 in
  [aeiou])
    echo "és una vocal"
    ;;
  )
  [bcdfghjklmnpqrstvwxyz]
    echo "és una consonant"
    ;;
  *)
    echo "és una altra cosa (no binari?)"
esac
exit 0




case $1 in
  "pere"|"pau"|"joan")
    echo "és un nen"
    ;;
  "marta"|"anna"|"julia")
    echo "és un nena"
    ;;
  *)
    echo "no binari"
    ;;
esac
exit 0


# DIFERENCIA QUE HAY ENTRE ELIF, IF Y ELSE
    # IF Y ELSE TENEMOS QUE PONER *CONDICIONES* 
# EN CASE LO QUE TENEMOS SON CONSTANTES QUE EQUIVALEN A PATRONES DE EXPRESIONES REGULARES --> NO SE PUEDEN PONDER CONDICIONES
