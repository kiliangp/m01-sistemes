#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple de processar arguments
# ----------------------------------------------------

echo '$*: ' $*		# SON LA LISTA DE ARGUMENTOS
echo '$@: ' $@		# SON LA LISTA DE ARGUMENTOS
echo '$#: ' $#		# LA QUANTITAT D'ARGUMENTS QUE HI HA
echo '$0: ' $0		# EL NOM DEL PROGRAMA
echo '$1: ' $1		# EL PRIMER ARGUMENTO
echo '$2: ' $2		# EL SEGUNDO ARGUMENTO
echo '$9: ' $9
echo '$10: ' ${10}	# CUANDO EL NOMBRE PUEDE DAR CONFUSIÓN LO PONDREMOS ENTRE {}	
echo '$11: ' ${11}
echo '$$: ' $$		# ES EL PID DEL PROGRAMA
nom="puig"
echo "${nom}deworld"

