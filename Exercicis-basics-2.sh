# !/bin/bash
# Autor: Kilian Guanoluisa Pionce
#
# Data: 20-03-2023
# Curs:ASIX
# Exercicis d'scripts bàsics-2 
# 
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# SI ITERAMOS LOS ELEMENTOS UNO A UNO --> FOR
# SI ITERAMOS POR UN ELEMENTO DEFINIDO --> WHILE

# PROCESAR ARGUMENTOS:

# 1) PROCESAR LOS ARGUMENTOS I MOSTRAR POR stdout SOLO LOS 4 O MÁS CARÁCTERES

ALL_ARG=$*

for arguments in $ALL_ARG
do
  echo $arguments | grep -E ".{4,}"
done
exit 0
