#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple VALIDAR: validamos el numero de argumentos, tenemos que verificar que el argumento sea un directorio
#   $ prog edat
# ---------------------------------------
# test -d /tmp; echo $? --> PARA COMPROBAR SI ES UN DIRECTORIO


# 1) Si nums args no és correcte plega 
ERR_NARGS=1

if [ $# -ne 1 ]
then
  echo "ERROR: número d'arguments invàlids"
  echo "USAGE: $0 dir/link/file"
  exit $ERR_NARGS
fi

# 2) VALIDACIÓ ARGUMENT

arg=$1
if [ -d $arg ]
then
  echo "$arg és un directori"
elif [ -f $arg ]
then
  echo "$arg és un regular file"
elif [ -l $arg ]
then
  echo "$arg és un link"
elif ! [ -e $arg ]
then
  echo "$arg no existeix"
fi
exit 0 
