#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple VALIDAR: validamos el numero de argumentos, tenemos que verificar que el argumento sea un directorio
#   $ prog edat
# ---------------------------------------
# test -d /tmp; echo $? --> PARA COMPROBAR SI ES UN DIRECTORIO

ERR_NARGS=1
ERR_NODIR=2

# 1) Si nums args no és correcte plegar
if [ $# -ne 1 ] 
then
  echo "Error: numero args incorrecte" # EN EL EXAMEN HAREMOS LAS FRASES MÁS DESARROLADAS
  echo "Usage: $0 dir "
  exit $ERR_NARGS	 		# SABEMOS QUE ES UN ERROR CUANDO ES != 0 			
fi

# Si no és un directori plegar 
if [ ! -d $1 ]
then
  echo "Error:el valor $1 no és un directori"
  echo "Usage: $0 dir"
  exit $ERR_NODIR
fi
# XIXA: LLISTAR EL DIRECTORI
dir=$1
ls $dir
exit 0 
