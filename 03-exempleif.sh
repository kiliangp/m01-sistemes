#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple if: indicar si és major d'edat
#   $ prog edat
# ----------------------------------------------------
# NOMS DE VARIABLES SIGNIFICATIVOS --> TENEMOS QUE PONER NOMBRES "BUENOS" --> EJEMPLO: llista_noms (SÍ); noms(NO) --> EN CASO DE HACER LA SEGUNDA OPCIÓN ESTARÁ SUSPENDIDO 
# NOMBRE DE LAS VARIABLES --> SIEMPRE TENEMOS QUE EMPEZAR EL NOMBRE DE LAS VARIABLES EN MINÚSCULAS
# NOMBRE DE LAS CONSTANTES --> SIEMPRE TENEMOS QUE EMPEZAR EL NOMBRE POR MAYÚSCULAS

# 1) VALIDAR QUE EXISTE UN ARGUMENTO 
if [ $# -ne 1 ] 
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat" 	# LE ESTAMOS DICIENDO COMO TIENE QUE SER EL MENSAJE --> # EL #0 SIGNIFICA EL NOMBRE DEL PROGRAMA
  exit 1			# EN ESTE CASO EL OUTPUT SERÁ ESTE:
fi					# Error: numero args incorrecte
					# Usage: 03-exempleif.sh edat 

# 2) XIXA
edat=$1       # LE ESTAMOS DICIENDO QUE EDAT SEA EL PRIMER ARGUMENTO
if [ $edat -ge 18 ]	# EN EL INTERIOR DE LOS "BRACKETS" SIEMPRE TIENEN QUE TENER DOS ESPACIOS (1 DELANTE Y 1 DETRÁS)
then
  echo "edat $edat és major d'edat"
fi
exit 0


# TENEMOS 2 ESTRUCTURAS DE BLOQUE
# 	1. PARA VER SI HEMOS RECIBIDO UN DATO (EN ESTE CASO LA EDAT)
# 	2. PARA HACER EL "echo"


# SIEMPRE QUE HAYA UN MENSAJE DE ERROR (error desqualificat) --> error que cuando se produzca no se podrá ejecutar el programa:
# 	1. MOSTRAREMOS UN MENSAJE DE ERROR
# 	2. MENSAJE USAGE 
# 	3. SALDREMOS CON UN EXIT 1

# [ $# -ne 1] --> LE ESTAMOS DICIENDO QUE SOLO HAYA UN ARGMUMENTO
#
#
# Operadors aritmetics:
# -eq = 
# -gt >
# -lt <
# -le <=
# -ne != 
# -ge >=i
# ---------------------
# Operadors lògics:
# -a --> AND
# -o --> OR
#  ! --> NOT
#  -n --> NOT 
# 
# QUEREMOS DECIR QUE LA VARIABLE $num TIENE QUE SER MÁS PEQUEÑO QUE 18 Y MÁS GRANDE QUE 1:
	# [ $num -ge 1 -a $num -lt  18 ]	--> -a= AND

# NOS DAN UNA VARIABLE edat=34 Y LE ESTAMOS DICIENDO QUE LOS MAYORES DE EDAD ENTRE 18 Y MÁS PEQUEÑO QUE 65: 
# [ $edat -ge 18 -a $edat -lt 65 ]; echo $?
# [ --> INCLUIDO
# [ [ --> EL SEGUNDO ES EXCLUIDO

# QUE LA EDAT NO SEA 25
# [ $edat -ne 25 ]; echo $?
#
#
#
# OTRO EJEMPLO QUE EL NOMBRE SEA DIFERENTE DE "marta" Y "ramon"
# nom="pere"
# [ $nom != "marta" -o $nom != "ramon ]; echo $?
