#! /bin/bash
# Kilian Steven Guanoluisa Pionce
# Febrer 2023
#
# Exemple if: indicar si és major d'edat amb else 
#   $ prog edat
# ---------------------------------------
#
#
if [ $# -ne 1 ] 
then
  echo "Error: numero args incorrecte"
  echo "Usage: $0 edat" 	# LE ESTAMOS DICIENDO COMO TIENE QUE SER EL MENSAJE --> # EL #0 SIGNIFICA EL NOMBRE DEL PROGRAMA
  exit 1			# EN ESTE CASO EL OUTPUT SERÁ ESTE:
fi

edat=$1
if [ $edat -lt 18 ]
then
  echo "edat $edat és menor d'edat"
elif [ $edat -lt 65 ]
then
  echo "edat $edat és de població activa"
else
  echo "edat $edat és jubilat"
fi
exit 0 
