# !/bin/bash
# Autor: Kilian Guanoluisa Pionce
#
# Data: 7-03-2023
# Curs:ASIX
# Exemples While
# 
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# SI ITERAMOS LOS ELEMENTOS UNO A UNO --> FOR

# 7) NUMERA Y MUESTRA EN MAYÚSCULAS LA ENTRADA ESTANDAR

num=1
while read -r line
do
  echo "$num: $line" | tr 'a-z' 'A-Z'
  ((num++))

done
exit 0 

#  6) MOSTRAR stdin LINIA A LINIA FINS AL
#       token FI

while read -r line
do
  if [ $line = "FI" ]
  then
    exit 1
  else
    echo "$line"
    shift
  fi 
done
exit 0 

# OTRA MANERA DE HACERLO

read -r line
while [ "$line" != "FI"]
do
  echo "$line"
  read -r line
done
exit 0

# 5) NUMERAR stdin LINIA A LINIA
num=1

while read -r line
do
  echo "$num: $line"
  ((num++))
done
exit 0


# 4) PROCESAR ENTRADA ESTANDARD LINIA A LINIA
while read -r line      # MIENTRAS HAYA XIXA EN LA ENTRADA ESTANDARD LO HACEMOS
do                      # VA PROCESANDO LINEAS HASTA QUE HAGAMOS UN "CTR+D" O CUANDO SE ACABE EL FICHERO
  echo "$line"
  shift
done
exit 0

# PODEMOS HACER ESTO:
    # bash 12-exemplewhile.sh < 12-exemplewhile.sh
    # ps | bash 12-exemplewhile.sh

# 3) ITERAR POR LA LISTA DE ARGUMENTOS

while [ -n "$1" ]
do
  echo "$1 $# $*"
  shift
done
exit 0 

# 2) DAMOS UN ARGUMENTO Y HACEMOS UNA CUENTA ATRÁS

MIN=0
NUM=$1

while [ $NUM -ge $MIN ]
do
  echo "$NUM"
  ((NUM--))
done
exit 0


# 1) CONTADOR

MAX=10
num=1

while [ $num -le $MAX ]
do 
  echo "$num"
  ((num++))
done
exit 0
