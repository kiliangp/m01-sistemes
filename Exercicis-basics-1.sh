# !/bin/bash
# Autor: Kilian Guanoluisa Pionce
# Data: 13-03-2023
# Curs:ASIX
# Exercicis d'scripts bàsics
# 
# --------------------------------------------------------------------------------------------------------------------------------------------------------------

# SI ITERAMOS LOS ELEMENTOS UNO A UNO --> FOR

# 10. HACER UN PROGRAMA QUE RECIBE COMO ARGUMENTO UN NÚMERO INDICATIVO DEL NÚMERO MÁXIMO
# DE LÍNIA A MOSTRAR. EL PROGRAMA PROCESA POR stdin LINIA A LINIA I MUESTRA NUMERADES UN MAXIMO 
# NUM LINIAS

num=1
MAX=$1

while read -r line
do
  if [ "$num" -le $MAX ]; 
  then
    echo "$num": "$line"

    if [ "$num" -eq $MAX ];
    then
      exit 1
    fi
  fi
  ((num++))
done





# 9. HACER UN PROGRAMA QUE RECIBE POR stdin NOMBRES DE USUARIO (uno por linia), SI EXISTEN
# EN EL SISTEMA (en el fichero /etc/passwd) MUESTRA EL NOMBRE POR stdout. SI NO EXISTEN LO MUESTRA POR stderr

while read -r line
do	
  grep -q "^$line:" /etc/passwd 

  if [ $? -eq 0 ]; 
  then
    echo $line
  else
    echo $line >> /dev/stderr
  fi	  
done
exit 0 

# 8. HACER UN PROGRAMA QUE RECIBE COMO ARGUMENTO NOMBRES DE USUARIO, SI YA EXISTEN EN EL SISTEMA
# (en el fichero /etc/passwd) MUESTRA EL NOMBRE POR STDOUT. SI NO EXISTEN LO MUESTRA POR stderr

for user_name in $*
do	
  grep -q "^$user_name:" /etc/passwd      # SI QUEREMOS QUE NO SALGA LA SALIDA PODEMOS HACER "grep -q"
  if [ $? -eq 0 ]; 
  then
    echo $user_name
  else
    echo $user_name >> /dev/stderr
  fi	  
done

# PARA QUE EL EJERCICIO FUNCIONE BIEN LO QUE TENDREMOS QUE HACER A LA HORA DE EJECUTAR EL PROGRAMA ES LO SIGUIENTE:
  # bash Exercicis-basics-1.sh root bin sssd patata hola 2> error.log
    # cat error.log -> Te aparecerán los usuarios que no existen 

# 7. PROCESAR LINIA A LINIA LA ENTRADA ESTÁNDARD, SI LA LINIA TIENE MAS DE 60 CARACTERES LO MUESTRA, SINO, NO.

while read -r line
do	
  num=$(echo "$line" | wc -c)
  if [ "$num" -gt 60 ]; 
  then
    echo $line
  fi	  
done

# EXERCICI 7 AMB EL GREP:

while read -r line
do
  echo "$line" | grep -Eq "^.{60,}$"        #   grep -q = SIRVE PARA QUE NO SALGA LA SALIDA (/dev/null)
  if [ $? -eq 0 ];
  then
    echo "$num"
  else
    echo "$num" >> /dev/stderr
  fi

done
exit 0

# 6. HACER UN PROGRAMA QUE RECIBA COMO ARGUMENTOS NOMBRES DE DIAS DE LA SEMANA Y MUESTRA
# CUANTOS DIAS ERAN LABORABLES Y CUANTOS FESTIVOS. SI EL ARGUMENTO NO ES UN DIA DE LA SEMANA 
# GENERA UN ERROR POR stderror

ERR_ARG=1

laborables=0
festius=0
for dies in $*
do 
  case $dies in
    "dilluns"|"dimarts"|"dimecres"|"dijous"|"divendres")
      ((laborables++))
      ;;
    "dissabte"|"diumenge")
      ((festius++))
      ;;
    *)
      echo "ERROR: Aquest nom no correspon al dia de la setmana"
      echo "Usage: els dies de la setmana són: dilluns, dimarts, dimecres, dijous, divendes, dissabte, diumenge"
      exit $ERR_ARG
esac
done
echo "Hi ha $laborables dia/es laborable/s"
echo "Hi ha $festius dia/es festiu/s"
exit 0



# 5. MOSTRAR LÍNEA A LÍNEA LA ENTRADA ESTANDARD, RECORTANDO SOLO LOS PRIMEROS 50 CARÁCTERES

while read -r line
do
  echo "$line" | cut -c1-50

done
exit 0 

# 4. HACER UN PROGRAMA QUE RECIBE COMO ARGUMENTO NÚMEROS DE MES(UNO O MÁS) Y 
# INDICA POR CADA ARGUMENTO RECIBIDO CUANTOS DIAS TIENE EL MES 


for mes in $*
do
  case $mes in 
    "2")
      dies=28
      ;;
    "4"|"6"|"9"|"11")
      dies=30
      ;;
    "1"|"3"|"5"|"7"|"8"|"10"|"12")
      dies=31
      ;;
esac
echo "$dies"
done
exit 0 


# 3. HACER UN CONTADOR DESDE CERO HASTA EL VALOR INDICADO POR EL ARGUMENTO RECIBIDO
    
    # CONSTANT = CUANDO EL VALOR SE QUEDA FIJO DURANTE EL PROGRAMA, NO CAMBIA
        # POR ESO LA VARIABLE "min" TIENE QUE IR EN MAYÚSCULAS
min=0 
NUM=$1 
while [ $min -le $NUM ] 
do   
  echo "$min"   
((min++)) 
done 
exit 0

# 2. MOSTRAR LOS ARGUMENTOS RECIBIDOS LÍNIA A LÍNEA, NUMERÁNDOLOS.
num=1
arguments=$*

for arg in $arguments
do
  echo "$num:$arg"
  ((num++))
done
exit 0

# 1) NUMERA Y MUESTRA LA ENTRADA ESTANDAR

num=1
while read -r line
do
  echo "$num: $line"
  ((num++))

done
exit 0 





